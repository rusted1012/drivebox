import Vue from 'vue';
import Vuex from 'vuex';
import {CommandInterface} from '@/interfaces/command.interface';

Vue.use(Vuex);


var ID = function() {
    // Math.random should be unique because of its seeding algorithm.
    // Convert it to base 36 (numbers + letters), and grab the first 9 characters
    // after the decimal.
    return '_' + Math.random().toString(36).substr(2, 9);
};

 const store =  new Vuex.Store({
    state: {
        items: [],
        pendingsCommand: {
            hour: 11,
            minute: 0,
            description: '',
            itemsDesc: [],
        },
        commands: [],
        pastCommands: [],
    },
    mutations: {

        initialiseStore(state) {
            // Check if the ID exists
            if(localStorage.getItem('store')) {
                this.replaceState(
                    Object.assign(state, JSON.parse(localStorage.getItem('store')))
                );
            }
        },

        setItems(state, items) {
            state.items = items;
        },

        addToPendingCommand(state, item) {

            let idx = state.pendingsCommand.itemsDesc.findIndex((elm) => {
                return elm.item.id == item.id;
            });

            if (idx != -1) {

                // let item = state.pendingsCommand.itemsDesc[idx].count++;
                Vue.set(state.pendingsCommand.itemsDesc[idx],
                    'count',
                    state.pendingsCommand.itemsDesc[idx].count + 1);
            } else {

                state.pendingsCommand.itemsDesc = [
                    ...state.pendingsCommand.itemsDesc,
                    {
                        item: item,
                        count: 1,
                    },

                ];
            }


        },
        clearPendingCommand(state) {
            state.pendingsCommand.itemsDesc = [];
        },
        decreasePendingCommandItem(state, item) {
            let idx = state.pendingsCommand.itemsDesc.findIndex((elm) => {
                return elm.item.id == item.id;
            });
            if (idx != -1) {

                if (state.pendingsCommand.itemsDesc[idx].count == 1) {
                    // remove item if count == 1

                    state.pendingsCommand.itemsDesc.splice(idx, 1);

                    state.pendingsCommand.itemsDesc = [
                        ...state.pendingsCommand.itemsDesc,
                    ];
                } else {
                    Vue.set(state.pendingsCommand.itemsDesc[idx],
                        'count',
                        state.pendingsCommand.itemsDesc[idx].count - 1);
                }
            }

        },

        addPendingCommandToCommand(state, command) {

            command.id = ID();
            state.commands.push(command);
            state.commands = state.commands.sort((c1: CommandInterface, c2: CommandInterface) => {

                let c1Time = c1.hour * 60 + c1.minute;
                let c2Time = c2.hour * 60 + c2.minute;

                if (c1Time > c2Time) {
                    return 1;
                } else {
                    return -1;
                }
            });
            state.pendingsCommand.itemsDesc = [];

        },


        addCommandToPendingCommand(state, command) {

            let idx = state.commands.findIndex((element) => {
                return element.id == command.id;
            });

            state.commands.splice(idx, 1);
            state.pendingsCommand = command;
        },

        doneCommand(state, command) {
            let idx = state.commands.findIndex((element) => {
                return element.id == command.id;
            });

            state.commands.splice(idx, 1);
        }


    },
    actions: {},
    modules: {},
});

store.subscribe((mutation, state) => {
    // Store the state object as a JSON string
    localStorage.setItem('store', JSON.stringify(state));
});

export default store;
