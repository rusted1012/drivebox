export interface ItemInterface {
    id: string;
    name: string;
    image: string;
}
