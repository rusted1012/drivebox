import Item from '@/components/Item.vue';

export interface CommandInterface {
    hour: number;
    minute: number;
    description: string;
    itemsDesc: [{
        count: number;
        item: Item;
    }];
}
