import Vue from 'vue';
import App from './App.vue';
import store from './store';
import './registerServiceWorker';
import vuetify from './plugins/vuetify';
// @ts-ignore

// import './assets/main.scss';

Vue.config.productionTip = false;




new Vue({
  store,
  vuetify,
  render: (h) => h(App),

  beforeCreate() {
    this.$store.commit('initialiseStore');
  }
}).$mount('#app');
